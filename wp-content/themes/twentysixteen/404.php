<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

<div class="e404">
	<div>
    	<h1>Page Not Found</h1>
    	<p>Sorry, but the page you were trying to view does not exist.</p>
    </div>
</div>

<?php get_footer(); ?>
