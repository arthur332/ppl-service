<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

    <?php 

    if (!empty($_GET['utm_term'])) {
        $label = $_GET['utm_term'];
    } elseif (!empty($_COOKIE['location'])) {
        $label = $_COOKIE['location'];
    } else {
        $label = 'mmo';
    };

    $utm = get_UTM($label);

    $zoom = (!empty($utm[4])) ? $utm[4] : 15;

    if ($label == 'mmo') {
        $utm[2] = "Московская область";
    };

    ?>

</main>
<!--/main-->

<!--footer-->
<footer id="map" class="main">

    <!--container-->
    <div class="container">

        <!--recall-form-->
        <div id="recall-form" class="recall-form">
            <ul>
                <li>
                    <?php $post = get_post( 213 ); ?>
                    <?php echo $post->post_content; ?>
                </li>
                <li>
                    <?php $post = get_post( 215 ); ?>
                    <?php echo $post->post_content; ?>
                </li>
                <li>
                    <?php $post = get_post( 217 ); ?>
                    <?php echo $post->post_content; ?>
                </li>
                <li>
                    <b></b>
                </li>
                <li>
                    <b></b>
                </li>
            </ul>
            <p>
                <?php $post = get_post( 269 ); ?>
                <?php echo $post->post_content; ?>
            </p>
            <form action="?" name="recall-form" id="recall_form" method="POST">
                <input type="text" name="name" placeholder="Имя">
                <input type="text" name="phone" placeholder="Телефон">
                <input type="button" id="sendbutton" class="btn btn-blue" value="Отправить">
            </form>
        </div>
        <!--/recall-form-->

    </div>
    <!--/container-->

</footer>
<!--/footer-->

<!--DEV JS-->
<!--<script src="js/main.js"></script>-->

<!--Production JS-->
<script src="<?php echo get_template_directory_uri(); ?>/js/main.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/scripts.js"></script>

<script src="//api-maps.yandex.ru/2.0/?load=package.standard&lang=ru-RU" type="text/javascript"></script>

<script>
    $(function () {

        /*
         Brand slider*/
        $('.brands .owl-carousel').owlCarousel({
            loop: true,
            margin: 10,
            dots: false,
            nav: true,
            navText: ['', ''],
            items: 8,
            autoplay:true,
            autoplayTimeout:3000,
            autoplayHoverPause:false
        });

        /*
         Service slider*/
        $('.service .owl-carousel').owlCarousel({
            loop: true,
            margin: 10,
            dots: false,
            nav: true,
            navText: ['', ''],
            items: 4
        });


        ymaps.ready(init);

        function init() {

            ymaps.geocode('<?php echo $utm[2]; ?>', {results: 1}).then(function (res) {
                // Выбираем первый результат геокодирования.
                var firstGeoObject = res.geoObjects.get(0),

                // Создаем карту с нужным центром.
                        myMap = new ymaps.Map("map", {
                            center: firstGeoObject.geometry.getCoordinates(),
                            zoom: <?php echo $utm[4]; ?>
                        });

                myMap.container.fitToViewport();

            });
        }

    });
</script>

</body>
</html>