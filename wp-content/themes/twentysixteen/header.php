<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="canonical" href="http://ppl.makefresh.org/"/>
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/android-icon-192x192.png" sizes="192x192" />
	<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/apple-icon-114x114.png" sizes="114x114" />
	<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/apple-icon-120x120.png" sizes="120x120" />
	<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/apple-icon-144x144.png" sizes="144x144" />
	<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/apple-icon-152x152.png" sizes="152x152" />
	<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/apple-icon-180x180.png" sizes="180x180" />
	<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/apple-icon-57x57.png" sizes="57x57" />
	<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/apple-icon-60x60.png" sizes="60x60" />
	<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/apple-icon-72x72.png" sizes="72x72" />
	<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/apple-icon-76x76.png" sizes="76x76" />
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" />
	<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src=/js/ie.min.js"></script>
    <![endif]-->

    

</head>

<?php
function get_UTM($current_label) {
    global $wpdb;
    $table_name = $wpdb->get_blog_prefix() . 'utm';
	$posts = $wpdb->get_results("SELECT pretext, name, map, label, zoom FROM $table_name;",ARRAY_A);
	foreach ($posts as $post) { setup_postdata($post);
		//var_dump($post);
		$label = $post['label'];
		if (preg_match("/".$label."/", $current_label)) {
			$utm[0]	= $post['pretext'];
			$utm[1]	= $post['name'];
			$utm[2]	= $post['map'];
			$utm[3]	= $post['label'];
            $utm[4] = $post['zoom'];
		};
	};
    if (isset($utm) && !empty($utm)) {
    	return $utm;
    } else {
    	$utm[0] = 'в';
    	$utm[1] = 'Москве и МО';
    	$utm[2] = 'Москве и МО';
    	$utm[3] = 'mmo';
        $utm[4] = '8';
    	return $utm;
    };
};
?>


<input type="hidden" name="utm_source" id="utm_source" value="<?php echo isset($_GET['utm_term']) ? $_GET['utm_term'] : '' ;?>">

	<?php 
	if (isset($_GET['utm_term']) && $_GET['utm_term']!='') {
		$label = $_GET['utm_term'];
	} elseif (isset($_COOKIE['location'])) {
		$label = $_COOKIE['location'];
	} else {
		$label = 'mmo';
	}
	?>
	<?php $utm = get_UTM($label); ?>
	<?php //echo $utm[0].' '.$utm[1]; ?>


<body <?php body_class(); ?>>

<!--noindex-->

<!--[if lt IE 9]>
<aside class="alert alert-browsehappy">
    <button type="button" class="close" data-dismiss="alert" area-hidden="true">&times;</button>
    Вы используете <strong>устаревший</strong> браузер. Мы не гарантируем корректное отображение сайта на вашем
    браузере, обоновите ваш браузер <a href="http://browsehappy.com/" rel="nofollow">тут</a>
</aside>
<![endif]-->

<!--/noindex-->

<!--header-->
<header class="main">

    <!--top-->
    <div class="top">

        <!--container-->
        <div class="container">
			<nav id="site-navigation" class="main-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Primary Menu', 'twentysixteen' ); ?>">
				<?php
					wp_nav_menu( array(
						'theme_location' => 'primary',
						'menu_class'     => 'primary-menu',
					 ) );
				?>
			</nav><!-- .main-navigation -->
        </div>
        <!--/container-->

    </div>
    <!--/top-->

    <!--bottom-->
    <div class="bottom">

        <!--container-->
        <div class="container">

            <!--logo-->
            <div class="logo-wrapper">
                <img src="<?php echo get_template_directory_uri(); ?>/images/ppl-logo.jpg" alt="logo">
            </div>
            <!--/logo-->

            <!--title-->
            <div class="title">
            	<?php $Page_ID = get_the_ID(); ?>
            	<?php $parent_id = get_ancestors( $Page_ID, 'page' );?>
        		<?php $parent = get_post($parent_id[0]); ?>
                <p>
                    <?php echo $parent->post_title; ?> <br>
                    <?php echo $utm[0]; ?> <span><?php echo $utm[1]; ?></span>
                </p>
            </div>
            <!--/title-->

            <!--time-->
            <div class="time">
            	<?php $post = get_post( 20 ); ?>
                <span><?php echo $post->post_content; ?></span>
            </div>
            <!--/time-->

            <!--tel-->
            <div class="phone">
                <?php $post = get_post( 22 ); ?>
                <span><?php echo $post->post_content; ?></span>
            </div>
            <!--/tel-->

            <!--btn-wrapper-->
            <div class="btn-wrapper">
                <a href="#recall-form" class="btn btn-blue btn-bold">Вызов мастера</a>
            </div>
            <!--/btn-wrapper-->

        </div>
        <!--/container-->

    </div>
    <!--/bottom-->

</header>
<!--/header-->

<!--main-->
<main>