$(document).ready(function(){

	$('body').on('click','#sendbutton',function(){
		var
			$form = $("#recall_form"),
			name = $form.find('input[name="name"]').val();
			phone = $form.find('input[name="phone"]').val();
			$.ajax({
				url: 'http://ppl.makefresh.org/wp-content/themes/twentysixteen/include/recall.php',
				type: 'POST',
				cache: false,
				data: {
					name : name,
					phone : phone
				},
				error: function(jqXHR, textStatus, errorThrown) {
					alert(textStatus, errorThrown);
				},
				success : function(data) {
				alert(data);
				}
			})

		return false;
	});

	var utm = $('#utm_source').val();
	if (utm == '') {
		var cookie_val = document.cookie;
		if (cookie_val == null) {
			$('#utm_source').val('mmo');
		} else {
			$('#utm_source').val(get_cookie('location'));
		};
	} else {
		var cookie_date = new Date();
		cookie_date.setDate ( cookie_date.getDate()+30 );
		document.cookie = "location="+utm+"; path=/; expires="+cookie_date.toGMTString();
	};

});

function get_cookie ( cookie_name )
{
  var results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' );
 
  if ( results )
    return ( unescape ( results[2] ) );
  else
    return null;
}