<?php 

$Page_ID = get_the_ID();
$parent_id = get_ancestors( $Page_ID, 'page' );
$parent_id = $parent_id[0];

if (!empty($parent_id)) {
    if ($parent_id == 13) {
        $class = 'wash';
    }
    if ($parent_id == 5) {
        $class = 'tech';
    }
    if ($parent_id == 7) {
        $class = 'plumber';
    }
    if ($parent_id == 9) {
        $class = 'electro';
    }
    if ($parent_id == 11) {
        $class = 'master';
    }
    $is_parent = false;
} else {
    if ($Page_ID == 13) {
        $class = 'wash';
    }
    if ($Page_ID == 5) {
        $class = 'tech';
    }
    if ($Page_ID == 7) {
        $class = 'plumber';
    }
    if ($Page_ID == 9) {
        $class = 'electro';
    }
    if ($Page_ID == 11) {
        $class = 'master';
    }
    $is_parent = true;
}

?>
<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

   <!--  <?php 
    if (isset($_GET['utm_term'])) {
        $label = $_GET['utm_term'];
    } elseif (isset($_COOKIE['location'])) {
        $label = $_COOKIE['location'];
    } else {
        $label = 'mmo';
    }
    ?>
    <?php $utm = get_UTM($label); ?> -->

    <!--container-->
    <div class="container">
        <?php $parent = get_post($parent_id); ?>
        <?php $current = get_post($Page_ID); ?>
        <!--section-menu-->
        <div class="section-menu <?php echo $class; ?>">
            <?php if ($is_parent) {
                $temp_id = $current->ID;
            } else {
                $temp_id = $parent->ID;
            }; ?>
            <?php submenu($temp_id); ?>
        </div>
        <!--/section-menu-->

        <!--banner-->
        <?php $thumbid = get_post_thumbnail_id( $Page_ID ); ?>
        <?php $src = wp_get_attachment_image_src( $thumbid, 'full'); ?>
        <section class="banner banner-<?php echo $class; ?>" <?php if (!empty($src)) {?> style="background: url('<?php echo $src[0]; ?>') no-repeat center !important;" <?php };?>>

            <hgroup>
                <h1><?php echo get_post_meta($Page_ID, 'add', true); ?></h1>

                <h2><?php echo $utm[0].' '.$utm[1]; ?></h2>
            </hgroup>

            <p><?php echo get_post_meta($Page_ID, 'add2', true); ?></p>

            <a href="#recall-form" class="btn btn-white btn-bold">Вызов мастера</a>
        </section>
        <!--/banner-->

        <?php 
        $brands = get_post_meta($Page_ID, 'brands', true);
        if (!empty($brands)) {
        ?>

        <!--brands-->
        <div class="brands">

            <!--owl-carousel-->
            <div class="owl-carousel">
                <?php echo $brands; ?>
            </div>
            <!--/owl-carousel-->

        </div>
        <!--/brands-->
        <?php
        }
        ?>

    </div>
    <!--/container-->


    
    	<?php $have_prices = false; ?>
    	<?php $show_preview = get_post_meta($Page_ID, 'show_preview', true); ?>
    	<?php for ($k = 1; $k <= 10; $k++) {
    		if (!empty(get_post_meta($Page_ID, 'service_name_'.$k, true)) && !empty(get_post_meta($Page_ID, 'service_price_'.$k, true))) {
    			$have_prices = true;
    		}
    	}; ?>
    	<?php if ($have_prices) { ?>
            <div class="container">
    		<section class="price <?php if (!$show_preview) {?>double<?php }; ?>">
    			<h2>Цены</h2>
		    	<?php for ($k = 1; $k <= 10; $k++) {
		    		if (!empty(get_post_meta($Page_ID, 'service_name_'.$k, true)) && !empty(get_post_meta($Page_ID, 'service_price_'.$k, true))) { ?>
				        <article>
				            <div <?php if ($show_preview) {?>class="container"<?php }; ?>>
				                <ul>
				                    <li><?php echo get_post_meta($Page_ID, 'service_name_'.$k, true); ?></li>
				                    <?php if ($show_preview) {?><li><?php echo get_post_meta($Page_ID, 'service_preview_'.$k, true); ?></li><?php }; ?>
				                    <li><?php echo get_post_meta($Page_ID, 'service_price_'.$k, true); ?></li>
				                </ul>
				            </div>
				        </article>
		    		<?php }
		    	}; ?>
    		</section>
            </div>
    	<?php } ?>
    

   <?php

function submenu($id) {
	if (!empty($id)) {
	    if ($id == 13) { ?> <!-- ремонт бытовой техники -->

            <figure class="washer">
                <a href="/remont-bytovoj-tehniki/stiralnaya-mashina/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'washer', true); ?></p>
            </figure>

            <figure class="frost">
                <a href="/remont-bytovoj-tehniki/holodilnik/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'frost', true); ?></p>
            </figure>

            <figure class="plate">
                <a href="/remont-bytovoj-tehniki/elektroplita/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'plate', true); ?></p>
            </figure>

            <figure class="plate">
                <a href="/remont-bytovoj-tehniki/gazovaya-plita/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'plate1', true); ?></p>
            </figure>

            <figure class="panel">
                <a href="/remont-bytovoj-tehniki/varochnaya-panel/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'panel', true); ?></p>
            </figure>

            <figure class="shelf">
                <a href="/remont-bytovoj-tehniki/duhovoj-shkaf/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'shelf', true); ?></p>
            </figure>

            <figure class="car-wash">
                <a href="/remont-bytovoj-tehniki/posudomoechnaya-mashina/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'car_wash', true); ?></p>
            </figure>

            <figure class="dry">
                <a href="/remont-bytovoj-tehniki/sushilki/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'dry', true); ?></p>
            </figure>

            <figure class="cond">
                <a href="/remont-bytovoj-tehniki/konditsioner/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'cond', true); ?></p>
            </figure>

            <figure class="cofe">
                <a href="/remont-bytovoj-tehniki/kofemashiny/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'cofe', true); ?></p>
            </figure>

            <figure class="svc">
                <a href="/remont-bytovoj-tehniki/vstroennaya-svch/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'svc', true); ?></p>
            </figure>

            <figure class="vent">
                <a href="/remont-bytovoj-tehniki/vytyazhka/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'vent', true); ?></p>
            </figure>

	    <?php } if ($id == 5) { ?> <!-- подключение бытовой техники -->

            <figure class="washer">
                <a href="/podklyuchenie-bytovoj-tehniki/stiralnaya-mashina/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'washer', true); ?></p>
            </figure>

            <figure class="frost">
                <a href="/podklyuchenie-bytovoj-tehniki/vstraivaemyj-holodilnik/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'frost', true); ?></p>
            </figure>

            <figure class="panel">
                <a href="/podklyuchenie-bytovoj-tehniki/varochnaya-panel/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'panel', true); ?></p>
            </figure>

            <figure class="shelf">
                <a href="/podklyuchenie-bytovoj-tehniki/duhovoj-shkaf/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'shelf', true); ?></p>
            </figure>

            <figure class="vent">
                <a href="/podklyuchenie-bytovoj-tehniki/vytyazhka/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'vent', true); ?></p>
            </figure>

            <figure class="svc">
                <a href="/podklyuchenie-bytovoj-tehniki/vstroennaya-svch/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'svc', true); ?></p>
            </figure>

            <figure class="car-wash">
                <a href="/podklyuchenie-bytovoj-tehniki/posudomoechnaya-mashina/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'car_wash', true); ?></p>
            </figure>

            <figure class="dry">
                <a href="/podklyuchenie-bytovoj-tehniki/sushilnaya-mashina/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'dry', true); ?></p>
            </figure>

            <figure class="plate">
                <a href="/podklyuchenie-bytovoj-tehniki/gazovaya-plita/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'plate', true); ?></p>
            </figure>

            <figure class="car">
                <a href="/podklyuchenie-bytovoj-tehniki/demontazh-tehniki/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'car', true); ?></p>
            </figure>

	    <?php } if ($id == 7) { ?> <!-- сантехник -->

            <figure class="toilet">
                <a href="/uslugi-santehnika/ustanovka-unitaza/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'toilet', true); ?></p>
            </figure>

            <figure class="bide">
                <a href="/uslugi-santehnika/ustanovka-bide-pisuara/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'bide', true); ?></p>
            </figure>

            <figure class="bath">
                <a href="/uslugi-santehnika/ustanovka-vanny/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'bath', true); ?></p>
            </figure>

            <figure class="jacuzzi">
                <a href="/uslugi-santehnika/ustanovka-dzhakuzi/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'jacuzzi', true); ?></p>
            </figure>

            <figure class="crane">
                <a href="/uslugi-santehnika/ustanovka-smesitelya/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'crane', true); ?></p>
            </figure>

            <figure class="shower">
                <a href="/uslugi-santehnika/ustanovka-dushevoj-kabiny/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'shower', true); ?></p>
            </figure>

            <figure class="sink">
                <a href="/uslugi-santehnika/ustanovka-rakoviny/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'sink', true); ?></p>
            </figure>

            <figure class="towel">
                <a href="/uslugi-santehnika/ustanovka-polotentsesushitelya/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'towel', true); ?></p>
            </figure>

            <figure class="clean">
                <a href="/uslugi-santehnika/ustranenie-zasorov/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'clean', true); ?></p>
            </figure>

            <figure class="construct">
                <a href="/uslugi-santehnika/montazh-truboprovodki/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'construct', true); ?></p>
            </figure>

            <figure class="warm">
                <a href="/uslugi-santehnika/montazh-otopleniya/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'warm', true); ?></p>
            </figure>

            <figure class="destruct">
                <a href="/uslugi-santehnika/demontazh-santehniki/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'destruct', true); ?></p>
            </figure>

	    <?php } if ($id == 9) { ?> <!-- электрик -->

            <figure class="switch">
                <a href="/uslugi-elektrika/ustanovka-vyklyuchatelej/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'switch', true); ?></p>
            </figure>

            <figure class="power">
                <a href="/uslugi-elektrika/ustanovka-rozetok/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'power', true); ?></p>
            </figure>

            <figure class="uzo">
                <a href="/uslugi-elektrika/ustanovka-avtomatov-uzo/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'uzo', true); ?></p>
            </figure>

            <figure class="chandelier">
                <a href="/uslugi-elektrika/montazh-lyustry/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'chandelier', true); ?></p>
            </figure>

            <figure class="light">
                <a href="/uslugi-elektrika/ustanovka-svetilnikov/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'light', true); ?></p>
            </figure>

            <figure class="cable">
                <a href="/uslugi-elektrika/prokladka-elektriki/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'cable', true); ?></p>
            </figure>

            <figure class="electro">
                <a href="/uslugi-elektrika/remont-elektriki/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'electro', true); ?></p>
            </figure>

            <figure class="cable-repair">
                <a href="/uslugi-elektrika/zamena-provodki/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'cable_repair', true); ?></p>
            </figure>

	    <?php } if ($id == 11) { ?> <!-- мастер на час -->

            <figure class="screwdriver">
                <a href="/master-na-chas/malyj-bytovoj-remont/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'screwdriver', true); ?></p>
            </figure>

            <figure class="furniture">
                <a href="/master-na-chas/demontazh-mebeli/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'furniture', true); ?></p>
            </figure>

            <figure class="floor">
                <a href="/master-na-chas/demontazh-pola/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'floor', true); ?></p>
            </figure>

            <figure class="door">
                <a href="/master-na-chas/demontazh-dverej/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'door', true); ?></p>
            </figure>

            <figure class="unfur">
                <a href="/master-na-chas/sborka-mebeli/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'unfur', true); ?></p>
            </figure>

            <figure class="repair">
                <a href="/master-na-chas/remont-mebeli/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'repair', true); ?></p>
            </figure>

            <figure class="cornice">
                <a href="/master-na-chas/ustanovka-karnizov-zhalyuzi/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'cornice', true); ?></p>
            </figure>

            <figure class="uncor">
                <a href="/master-na-chas/demontazh-karnizov-zhalyuzi/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'uncor', true); ?></p>
            </figure>

            <figure class="decor">
                <a href="/master-na-chas/naveska-dekora/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'decor', true); ?></p>
            </figure>

            <figure class="rclean">
                <a href="/master-na-chas/uborka-posle-remonta/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'rclean', true); ?></p>
            </figure>

	    <?php }
	}
};

?>

<?php get_sidebar(); ?>
<?php get_footer(); ?>
