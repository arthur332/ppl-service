<?php
/**
 * The template for the sidebar containing the main widget area
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

    <!--advantages-->
    <div class="advantages">
        <!--container-->
        <div class="container">
        	<?php $id = 25; ?>
        	<ul>
        		<li><?php echo get_post_meta($id, 'years', true); ?></li>
        		<li><?php echo get_post_meta($id, 'depart', true); ?></li>
        		<li><?php echo get_post_meta($id, 'diag', true); ?></li>
        		<li><?php echo get_post_meta($id, 'guarantee', true); ?></li>
        	</ul>
        </div>
        <!--/container-->
    </div>
    <!--/advantages-->

    <!--guarantee-->
    <section class="guarantee">

        <!--container-->
        <div class="container">
        	<?php $id = 27; ?>
            <!--left-->
            <div class="left">
				<?php $post = get_post( $id ); ?>
				<h2><?php echo $post->post_title; ?></h2>
				<p><b>Гарантия <span>"СТАНДАРТ"</span></b> - <?php echo get_post_meta($id, 'standart', true); ?></p>
				<p><b>Расширенная гарантия <span>"VIP"</span></b> - <?php echo get_post_meta($id, 'vip', true); ?></p>
				<a href="#recall-form" class="btn btn-blue">Получить расширеную гарантию</a>
				<p><?php echo get_post_meta($id, 'add', true); ?></p>
            </div>
            <!--/left-->

            <!--right-->
            <aside class="right">
            	<?php $thumbid = get_post_thumbnail_id( $id ); ?>
            	<?php $src = wp_get_attachment_image_src( $thumbid, 'full'); ?>
                <img src="<?php echo $src[0]; ?>" alt="guarantee">
            </aside>
            <!--/right-->

        </div>
        <!--/container-->

    </section>
    <!--/guarantee-->

    <!--about-->
    <section class="about">
        <!--container-->
        <div class="container">

            <!--director-->
            <aside class="director">
            	<?php $id = 30; ?>
				<?php $thumbid = get_post_thumbnail_id( $id ); ?>
            	<?php $src = wp_get_attachment_image_src( $thumbid, 'full'); ?>
                <img src="<?php echo $src[0]; ?>" alt="director">

                <p>
					<?php $post = get_post( $id ); ?>
        			<?php echo $post->post_content; ?>
                </p>
            </aside>
            <!--/director-->

            <!--body-->
            <div class="body">
            	<?php $id = 33; ?>
            	<?php $post = get_post( $id ); ?>
                <h2><?php echo $post->post_title; ?></h2>
        		<?php echo $post->post_content; ?>

        		<?php $thumbid = get_post_thumbnail_id( $id ); ?>
            	<?php $src = wp_get_attachment_image_src( $thumbid, 'full'); ?>
                <?php echo get_the_post_thumbnail($id, 'thumbnail'); ?>
                <?php 
				if (class_exists('MultiPostThumbnails')) : 
				MultiPostThumbnails::the_post_thumbnail(get_post_type($id), 'secondary-image');
				endif;
				?>
                <?php 
				if (class_exists('MultiPostThumbnails')) : 
				MultiPostThumbnails::the_post_thumbnail(get_post_type($id), 'thirdary-image');
				endif;
				?>

            </div>
            <!--/body-->

        </div>
        <!--/container-->

        <!--skills-->
        <div class="skills">

            <!--container-->
            <div class="container">
            	<?php $id = 38; ?>
            	<figure>
            		<b><?php echo get_post_meta($id, '1.1', true); ?></b>
            		<p><?php echo get_post_meta($id, '1.2', true); ?></p>
            	</figure>
            	<figure>
            		<b><?php echo get_post_meta($id, '2.1', true); ?></b>
            		<p><?php echo get_post_meta($id, '2.2', true); ?></p>
            	</figure>
            	<figure>
            		<b><?php echo get_post_meta($id, '3.1', true); ?></b>
            		<p><?php echo get_post_meta($id, '3.2', true); ?></p>
            	</figure>
            	<figure>
            		<b><?php echo get_post_meta($id, '4.1', true); ?></b>
            		<p><?php echo get_post_meta($id, '4.2', true); ?></p>
            	</figure>
            </div>
            <!--/container-->

        </div>
        <!--/skills-->

    </section>
    <!--/about-->

    <!--service-->
    <section class="service">

        <!--container-->
        <div class="container">
        	<?php $id = 44; ?>
            <!--left-->
            <?php $thumbid = get_post_thumbnail_id( $id ); ?>
            <?php $src = wp_get_attachment_image_src( $thumbid, 'full'); ?>
            <aside class="left">
                <img src="<?php echo $src[0]; ?>" alt="service">
            </aside>
            <!--/left-->

            <!--right-->
            <div class="right">
            	<?php $post = get_post( $id ); ?>
                <h2><?php echo $post->post_title; ?></h2>

                <p>
                	<?php echo $post->post_content; ?>
                </p>

                <?php $post = get_post( 22 ); ?>
                <a href="#recall-form" class="btn btn-white btn-bold">Оставить заявку</a>
                <p class="btn btn-blue btn-bold"><?php echo $post->post_content; ?></p>

                <!--owl-carousel-->
                <div class="owl-carousel">
                	<?php $id = 47; ?>
            		<?php $post = get_post( $id ); ?>
            		<?php echo $post->post_content; ?>
                </div>
                <!--/owl-carousel-->

            </div>
            <!--/right-->

        </div>
        <!--/container-->

    </section>
    <!--/service-->

    <!--reviews-->
    <div class="reviews">

        <!--container-->
        <div class="container">

            <?php
            if ( have_posts() ) :
              query_posts('cat=5');
              while (have_posts()) : the_post();
            ?>
                <article>
                    <p>
                        <?php the_content();?>
                    </p>
                    <footer>
                        <span><?php echo get_the_date(); ?> <?php the_title(); ?></span>
                    </footer>
                </article>
            <?php 
               endwhile; 
            endif;
            wp_reset_query();
            ?>

        </div>
        <!--/container-->

    </div>
    <!--/reviews-->

<?php /*
<?php if ( is_active_sidebar( 'sidebar-1' )  ) : ?>
	<aside id="secondary" class="sidebar widget-area" role="complementary">
		<?php dynamic_sidebar( 'sidebar-1' ); ?>
	</aside><!-- .sidebar .widget-area -->
<?php endif; ?>
*/?>
