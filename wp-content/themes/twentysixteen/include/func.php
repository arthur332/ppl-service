<?php

function submenu($id) {
	if (!empty($id)) {
	    if ($id == 13) { ?> <!-- ремонт бытовой техники -->

            <figure class="washer">
                <a href="/remont-bytovoj-tehniki/stiralnaya-mashina/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'washer', true); ?></p>
            </figure>

            <figure class="frost">
                <a href="/remont-bytovoj-tehniki/holodilnik/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'frost', true); ?></p>
            </figure>

            <figure class="plate">
                <a href="/remont-bytovoj-tehniki/elektroplita/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'plate', true); ?></p>
            </figure>

            <figure class="plate">
                <a href="/remont-bytovoj-tehniki/gazovaya-plita/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'plate1', true); ?></p>
            </figure>

            <figure class="panel">
                <a href="/remont-bytovoj-tehniki/varochnaya-panel/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'panel', true); ?></p>
            </figure>

            <figure class="shelf">
                <a href="/remont-bytovoj-tehniki/duhovoj-shkaf/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'shelf', true); ?></p>
            </figure>

            <figure class="car-wash">
                <a href="/remont-bytovoj-tehniki/posudomoechnaya-mashina/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'car_wash', true); ?></p>
            </figure>

            <figure class="dry">
                <a href="/remont-bytovoj-tehniki/sushilki/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'dry', true); ?></p>
            </figure>

            <figure class="cond">
                <a href="/remont-bytovoj-tehniki/konditsioner/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'cond', true); ?></p>
            </figure>

            <figure class="cofe">
                <a href="/remont-bytovoj-tehniki/kofemashiny/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'cofe', true); ?></p>
            </figure>

            <figure class="svc">
                <a href="/remont-bytovoj-tehniki/vstroennaya-svch/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'svc', true); ?></p>
            </figure>

            <figure class="vent">
                <a href="/remont-bytovoj-tehniki/vytyazhka/" class="a-wrapper"></a>

                <p><?php echo get_post_meta($id, 'vent', true); ?></p>
            </figure>

	    <?php } if ($id == 5) { ?> <!-- подключение бытовой техники -->



	    <?php } if ($id == 7) { ?> <!-- сантехник -->



	    <?php } if ($id == 9) { ?> <!-- электрик -->



	    <?php } if ($id == 11) { ?> <!-- мастер на час -->



	    <?php }
	}
};

?>